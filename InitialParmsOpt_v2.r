# InitialParmsOpt_v2.R
# changes summer and winter periods

library(popbio)      # library for matrix model analyses
library(gtools)

# DEMOGRAPHIC RATES
#... natural survival
phi = array(0,dim=c(3,2,2))
phi.max = 0.88 # maximum annual survival (all birds aged 0.5+ in fall) (from GenParms.r)
phi.max.prop = 0.9 # proportion of phi.max to make juvenile winter survival less for MU1
                   # based on Pristorius 2006, Schneider 2022
phi[,2,2] = phi.max^(6/12) #MU2
phi[,2,1] = phi.max^(6/12) #MU1
phi[,1,1:2] = phi.max^(6/12)
phi[1,1,1] = phi[1,1,1]*phi.max.prop

#... recruitment rate
#...... post-breeding repro rate under optimal conditions for geese aged 3+ (given phi=0.88, lambda=1.16 from GemParms.r)
#fcn = function(x,phi=phi.max) 1.16-as.numeric(eigen(matrix(c(0,0,phi*x,phi,0,0,0,phi,phi),nrow=3,byrow=T))$values[1])
#... from 2022 status report, but lambda=1.01 includes harvest! Assume h=0.1
fcn = function(x,phi=phi.max*0.90) 1.014-as.numeric(eigen(matrix(c(0,0,phi*x,phi,0,0,0,phi,phi),nrow=3,byrow=T))$values[1])
alpha.max = round(uniroot(fcn,c(0,1))$root,2)
alpha.max.prop = 1 # proportion of alpha.max
alpha2 = alpha.max                  #MU2
alpha1 = alpha.max * alpha.max.prop #MU1

Z = matrix(c(0,0,phi.max*alpha.max,phi.max,0,0,0,phi.max,phi.max),nrow=3,byrow=T) # annual fall to fall matrix
SAD = stable.stage(Z)

#... MU breeding fidelity
#...... per E. Schneider 5/2/2022 (Leo email) for time interval starting 2006
pi1 = 1.00
pi2 = 1.00 
# pi2=0.73 from Schneider email (French Results.pdf) does not reflect that individuals
# switching units usually return to original unit (Schneider thesis)

# these were adjusted on 12Mar2023 to use only Dec-Feb records (i.e., true wintering birds)
#... proportion of MU1 wintering in North (1)
psi1 = 0.67 # from Leo Bacon "Greylag winter distribution.xlsx"
#... proportion of MU2 wintering in North (1)
psi2 = 0.95 # from Leo Bacon "Greylag winter distribution.xlsx"

# INITIAL POP SIZES (in thousands)
#... based on CurVal mean reported breeding pairs, b, 
# and Adults=b*2*phi.max^(4/12) in fall and SAD
# from GGDVCV breeding numbers (1).xlsx
MU1bp = 83.7
MU2bp = 139.4
Y1 = MU1bp*2*phi.max^(6/12)*alpha.max
Y2 = MU2bp*2*phi.max^(6/12)*alpha.max
T1 = Y1/SAD[1]
T2 = Y2/SAD[1]
N1 = matrix(c(T1*SAD[1],T1*SAD[2],T1*SAD[3]),nrow=3)
N2 = matrix(c(T2*SAD[1],T2*SAD[2],T2*SAD[3]),nrow=3)

# SAD.dir = rdirichlet(1,SAD*10)
# T1 = Y1/SAD.dir[1]
# T2 = Y2/SAD.dir[1]
# N1 = matrix(c(T1*SAD.dir[1],T1*SAD.dir[2],T1*SAD.dir[3]),nrow=3)
# N2 = matrix(c(T2*SAD.dir[1],T2*SAD.dir[2],T2*SAD.dir[3]),nrow=3)

# alternative initial pop based on some harvest (makes little difference)
# h = 0.08
# Z1 = matrix(c(0,0,(1-2*h)*phi.max*alpha.max,
#              (1-h)*phi.max,0,0,
#              0,(1-h)*phi.max,(1-h)*phi.max),
#            nrow=3,byrow=T) # annual fall to fall matrix
# SAD1 = stable.stage(Z1)

