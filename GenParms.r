# GenParms.r

rm(list=ls())

setwd(paste(getwd(),'/European Geese/D Greylag Geese/GG MU Modeling/',sep=''))
source('C:/Users/fjohn/OneDrive/Documents/MyFunctions.R')

mean.var = function(mu1,sd1,mu2,sd2) mean(sd1^2,sd2^2) + ((mu1-mu2)/2)^2

# survival function Johnson et al. 2012
s.fcn = function(mass,p,alpha) p^(1/(exp(3.22+0.24*log(mass)+rnorm(z,0,sqrt(0.087)))-alpha))
# growth function Niel & Lebreton 2005, Eq. 15
getlam1 = function(a,s) { ((s*a-s+a+1)+sqrt((s-s*a-a-1)^2-4*s*a^2))/(2*a) }

###########################################################################################################
z = 100000 # no. of samples

# Survival calculations
#alpha = 2.8 # age at first breeding
alpha = 3
massm.mean = 3.509 # male mass (Dunning 2008)
massm.sd = 0.321
massf.mean = 3.108 # female mass (Dunning 2008)
massf.sd = 0.274
mass.mean = mean(c(massm.mean,massf.mean))
mass.sd = sqrt(mean.var(massm.mean,massm.sd,massf.mean,massf.sd))

# ... replicates of body mass
gamma.parm = MOM.gamma(mass.mean,mass.sd^2)
set.seed(123)
mass = rgamma(z,shape=gamma.parm$shape,rate=gamma.parm$rate) # generate mass replicates

# ... replicates of proportion alive at max longevity (Johnson et al. 2012)
set.seed(123456)
p = rbeta(z,3.34,101.24)

# ... replicates of survival 
sb = s.fcn(mass,p,alpha)
mean(sb)
quantile(sb,probs=c(0.025,0.50,0.975))
hist(sb,xlim=c(0.5,1),col='gray',freq=F,xlab='Natural survival',main='',breaks=30,las=1);
abline(v=median(sb),lwd=2);grid()

# lam is the analytical solution for Niel & Lebreton equation 15,
# given age at first breeding (a) and adult survival (s)
lam = (getlam1(alpha,sb))
mean(lam)
quantile(lam,probs=c(0.025,0.50,0.975))
hist(lam,col='gray',freq=F,breaks=30,las=1,main='',xlab=expression(lambda));grid();box();abline(v=median(lam),lwd=2);

# fecundity (!!ONLY!! valid for scalar model)
# InitialParmsOpt.R finds the appropriate value for a matrix model
f = lam/sb - 1
mean(f)
quantile(f,probs=c(0.025,0.50,0.975))
hist(f,col='gray',freq=F,breaks=30,las=1,main='',xlab=expression(gamma))
grid()
box()
abline(v=median(f),lwd=2)

